# RcManagement

RcManagement est une application qui permet d'effectuer toute la gestion de votre saison sportive automobile.

# Ces fonctionnalités
RcManagement permet :

- la saisie des informations sur vos voitures de course (nom, marque, numéro de passeport ...)
- la saisie des pièces en stock pour chaque voiture,
- le suivi des pièces à changer en fonction du nombre d'épreuve et des kilimètre,
- les travaux à réliser sur chaque voiture (avec l'échéance, les pièces à changer ...),
- la saisie des jantes en stock en fonction de chaque voiture,
- la saisie des pneux en stock fonction de chaque voiture,
- la saisie des épreuves prévues au programme pour chaque saison ainsi que les essais programmés 

Elle est publiée sous licence GNU.

### Préparation et installation de l'application
```
docker pull jpayet43/rcmanagement{TAG}
docker
```

### Déploiement de la base données
```
bin/console doctrine:schema:update --force
```

### Variables d'environnement pour la mise en production (fichier .env à créer à la racine du dossier)
```
DATABASE_DRIVER=pdo_mysql
DATABASE_URL="mysql://mtracingcar:monpassword@172.17.0.3:3306/mtracingcar"
WKHTMLTOPDF_PATH=/usr/local/bin/wkhtmltopdf
WKHTMLTOIMAGE_PATH=/usr/local/bin/wkhtmltoimage

```


### Remerciements et crédits


### Soutien
Pour ton contact mail=tmskarting@gmail.com

