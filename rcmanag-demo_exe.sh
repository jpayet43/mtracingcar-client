#!/bin/bash

echo  "Démarrage de la base de données ... "
sleep 5

docker restart adminer_mysql57_1

[ $? -ne 0 ] && echo " ERREUR, lors du lancement du container" && exit 1

sleep 10
echo "OK"

# Script pour lancer un container docker de l'application

NAME=demo
APP=$(basename `pwd`)
DOCKER_NAME=${NAME}-$APP
IMAGE=jpayet43/rcmanagement:version-demo
VOLUME_DIR1=/home/volume/rcm_demo/cars
VOLUME_DIR2=/home/volume/rcm_demo/documents

docker stop ${DOCKER_NAME} >/dev/null 2>&1
docker rm ${DOCKER_NAME}   >/dev/null 2>&1

echo  "Lancement du container $DOCKER_NAME sur le port 8080... "
sleep 5

# pull de l'image de base
docker pull $IMAGE

#docker run -d --rm --name ${DOCKER_NAME} -v $(pwd):/sources --env-file=.env -p 8080 $IMAGE
docker run --restart always -p 8081:8080 -d \
  -v ${VOLUME_DIR1}:/sources/public/images/cars:rw \
  -u www-data \
  -v ${VOLUME_DIR2}:/sources/public/uploads:rw \
  -u www-data \
  --name ${DOCKER_NAME} $IMAGE

#docker run -p 8080:8080 -d --name ${DOCKER_NAME} -e DATABASE_HOST=192.168.122.10 -e DATABASE_PORT=3406 -e DATABASE_NAME=mtracingcar -e DATABASE_USER=mtracingcar -e DATABASE_PASSWORD=mtracingcar $IMAGE

[ $? -ne 0 ] && echo " ERREUR, lors du lancement du container" && exit 1

sleep 5
echo "OK"

sleep 5
echo "👍 Le container est maintenant lancé"
echo "##################################################################################"
echo "Execution du fichier startconfig.sh pour la configuration de l'environnement"
docker exec -it ${DOCKER_NAME} sh startconfig.sh
sleep 3
echo "👍 Environnement ok"
echo "##################################################################################"
echo "👉 Pour finaliser votre installation"
echo "👉 lancer la commande suivante : docker exec -it ${DOCKER_NAME} bash"
echo "👉taper : vi .env et aller à la ligne 32, et modifier votre mot de passe de la base de données"

# Proxy
PROXY=""
[ ! -z "$https_proxy" ] && PROXY="export https_proxy=$https_proxy" && YARN_PROXY="yarn config set https-proxy $https_proxy" &&  echo "Utilistion du proxy $https_proxy"
[ -z "$PROXY" ] && PROXY="unset https_proxy"


#echo -n "Maj database schema ... "
#docker exec ${DOCKER_NAME} bin/console d:s:u --force
#echo " OK"

PORT=$(docker port $DOCKER_NAME | cut -d: -f2)
